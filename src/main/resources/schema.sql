DROP TABLE people IF EXISTS;

CREATE TABLE trades  (
    trade_id BIGINT IDENTITY NOT NULL PRIMARY KEY,
    stock VARCHAR(20),
    time VARCHAR(100) ,
    price DECIMAL(10,2),
    shares DECIMAL(10,2)
);
