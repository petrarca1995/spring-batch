package com.example.demo;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    public JobBuilderFactory jobBuilderFactory;

    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public BatchConfig(JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory){
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }



    @Bean
    public FlatFileItemReader<Trade> reader() {
        return new FlatFileItemReaderBuilder<Trade>()
                .name("personItemReader")
                .resource(new ClassPathResource("trades.csv"))
                .delimited()
                .names(new String[]{"stock", "time","price","shares"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Trade>() {{
                    setTargetType(Trade.class);
                }})
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<Trade> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Trade>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO trades (stock, time, price, shares) VALUES (:stock, :time, :price, :shares)")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }




    @Bean
    public Step step1(JdbcBatchItemWriter<Trade> writer) {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(4);
        taskExecutor.setMaxPoolSize(4);
        taskExecutor.afterPropertiesSet();

        return stepBuilderFactory.get("step1")
                .<Trade, Trade> chunk(10)
                //* chunk() returns a SimpleStepbuilder
                .reader(reader())
            //    .processor(processor())
                .writer(writer)
                .taskExecutor(taskExecutor)
                .build();
        //* Build returns a TaskletStep, which is a Step
    }



}
